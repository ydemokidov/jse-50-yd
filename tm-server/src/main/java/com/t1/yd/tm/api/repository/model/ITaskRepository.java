package com.t1.yd.tm.api.repository.model;

import com.t1.yd.tm.model.Task;
import org.apache.ibatis.annotations.Param;
import org.jetbrains.annotations.NotNull;

import java.util.List;

public interface ITaskRepository extends IUserOwnedRepository<Task> {

    @NotNull List<Task> findAllByProjectId(@Param("userId") @NotNull String userId, @Param("projectId") @NotNull String projectId);

    @NotNull List<Task> findAll(@NotNull String sort);

}
