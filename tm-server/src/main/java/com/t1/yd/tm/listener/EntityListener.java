package com.t1.yd.tm.listener;

import com.t1.yd.tm.dto.logger.OperationEventDTO;
import com.t1.yd.tm.enumerated.OperationType;
import org.hibernate.event.spi.*;
import org.hibernate.persister.entity.EntityPersister;
import org.jetbrains.annotations.NotNull;

public class EntityListener implements PostInsertEventListener, PostDeleteEventListener, PostUpdateEventListener {

    private final JmsLogProducer jmsLogProducer;

    public EntityListener(@NotNull final JmsLogProducer jmsLogProducer) {
        this.jmsLogProducer = jmsLogProducer;
    }


    @Override
    public void onPostDelete(@NotNull final PostDeleteEvent event) {
        log(OperationType.DELETE, event.getEntity());
    }

    @Override
    public void onPostInsert(@NotNull final PostInsertEvent event) {
        log(OperationType.INSERT, event.getEntity());
    }

    @Override
    public void onPostUpdate(@NotNull final PostUpdateEvent event) {
        log(OperationType.UPDATE, event.getEntity());
    }

    @Override
    public boolean requiresPostCommitHanding(@NotNull final EntityPersister persister) {
        return false;
    }

    private void log(@NotNull final OperationType type, @NotNull final Object entity) {
        jmsLogProducer.send(new OperationEventDTO(type, entity));
    }


}
