package com.t1.yd.tm.exception.user;

public class AccessDeniedException extends AbstractUserException {

    public AccessDeniedException() {
        super("Error! Access is denied...");
    }

}
