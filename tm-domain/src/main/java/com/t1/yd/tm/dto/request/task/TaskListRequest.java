package com.t1.yd.tm.dto.request.task;

import com.t1.yd.tm.dto.request.AbstractUserRequest;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TaskListRequest extends AbstractUserRequest {

    String sort;

}
