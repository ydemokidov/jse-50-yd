package com.t1.yd.tm.dto.response.data;

import com.t1.yd.tm.dto.response.AbstractResultResponse;
import org.jetbrains.annotations.Nullable;

public class DataBase64LoadResponse extends AbstractResultResponse {

    public DataBase64LoadResponse(@Nullable final Throwable throwable) {
        super(throwable);
    }

    public DataBase64LoadResponse() {

    }
}
