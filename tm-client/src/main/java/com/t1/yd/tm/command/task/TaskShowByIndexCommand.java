package com.t1.yd.tm.command.task;

import com.t1.yd.tm.dto.model.TaskDTO;
import com.t1.yd.tm.dto.request.task.TaskShowByIndexRequest;
import com.t1.yd.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class TaskShowByIndexCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task_show_by_index";

    @NotNull
    public static final String DESCRIPTION = "Show task by Index";

    @Override
    public void execute() {
        System.out.println("[SHOW TASK BY ID]");
        System.out.println("ENTER INDEX:");

        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final TaskShowByIndexRequest request = new TaskShowByIndexRequest();
        request.setIndex(index);
        request.setToken(getToken());

        @Nullable final TaskDTO taskDTO = getTaskEndpointClient().showTaskByIndex(request).getTaskDTO();
        showTask(taskDTO);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
