package com.t1.yd.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.t1.yd.tm.api.service.ILoggerService;
import lombok.SneakyThrows;
import org.bson.Document;
import org.jetbrains.annotations.NotNull;

import java.util.LinkedHashMap;
import java.util.Map;

public class LoggerService implements ILoggerService {

    private final ObjectMapper objectMapper = new ObjectMapper();

    @NotNull
    private final MongoClient mongoClient = MongoClients.create("mongodb://localhost:27017");

    @NotNull
    private final MongoDatabase mongoDatabase = mongoClient.getDatabase("task_manager_log");

    @Override
    @SneakyThrows
    public void log(@NotNull final String text) {
        final Map<String, Object> event = objectMapper.readValue(text, LinkedHashMap.class);
        final String table = event.get("table").toString();
        @NotNull final MongoCollection<Document> collection = mongoDatabase.getCollection(table);
        collection.insertOne(new Document(event));
    }


}
